﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BurnoutVaultEditor
{
    public interface ILoader
    {
        void SetProgress(int progress);
        void SetTask(string task);
        void Finish();
    }
}
