﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BurnoutVaultEditor
{
    public partial class Loader : Form, ILoader
    {
        public delegate void OnCancel();
        public event OnCancel Cancelled;

        private bool _success;

        public Loader()
        {
            InitializeComponent();

            _success = false;
        }

        private void Loader_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_success)
                Cancelled?.Invoke();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private delegate void SetProgressDelegate(int progress);
        public void SetProgress(int progress)
        {
            if (pbProgress.InvokeRequired)
            {
                pbProgress.Invoke(new SetProgressDelegate(SetProgress), progress);
            }
            else
            {
                pbProgress.Value = progress;
            }
        }

        private delegate void SetTaskDelegate(string task);
        public void SetTask(string task)
        {
            if (lblLoading.InvokeRequired)
            {
                lblLoading.Invoke(new SetTaskDelegate(SetTask), task);
            }
            else
            {
                lblLoading.Text = task;
            }
        }


        private delegate void FinishDelegate();
        public void Finish()
        {
            if (InvokeRequired)
            {
                Invoke(new FinishDelegate(Finish));
            }
            else
            {
                _success = true;
                Close();
            }
        }
    }
}
