﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BurnoutVaultEditor
{
    public static class Program
    {
        public static Control Invoker;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Invoker = new Control();
            Invoker.CreateControl();

            MainForm form = new MainForm();
            if (args.Length > 0)
                form.DoOpen(args[0]);
            Application.Run(form);
        }
    }
}
